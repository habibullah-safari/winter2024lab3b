public class Horse {

	public String type;
	public boolean racer;
	public int maxLifeSpan;	
	
	public void priceCategory(){
		if(this.racer){
			System.out.println(" That's some expensive horse you got there !!!");
		}else{
			System.out.println(" Regular horse still cute ");
		}
	}

	public void funFacts(){
		if (this.type.equals("Arabian")){
			System.out.println(" Did you know that they weigh  between 800 to 1000 lbs ? ");
		}else {
			System.out.println(" Horses have a nearly 360-degree field of vision ");
		}
	}
}